import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import api from '../../services/api';

import { CardGrid, Card, Header, Menu, BorderBox, PageNumber, Spinner } from './styles';

function getClickablePages(actualPage) {
  const offsets = [0, 1, 2, 3, 4];
  return offsets.map(number => parseInt(actualPage, 10) + number);
}

function getPage(direction, actualPage) {
  const nextPage = parseInt(actualPage, 10) + direction;

  return nextPage >= 0 ? nextPage : 1;
}

export default function Home({ match }) {
  const [characters, setCharacters] = useState([]);
  const [loading, setLoading] = useState(false);
  const [speciesChecked, setSpecies] = useState([]);
  const [genderChecked, setGender] = useState([]);
  const clickablePages = getClickablePages(Number(match.params.page) || 1);
  const page = Number(match.params.page) || 1;

  useEffect(() => {
    async function loadData() {
      setLoading(true);
      const apiResponse = await api.get(`/?page=${page}`);

      setCharacters(apiResponse.data.results);
      setLoading(false);
    }

    loadData();
  }, [match.params.page, page, speciesChecked]);

  function setDisplayEpisodes(id) {
    setCharacters(
      characters.map(char =>
        char.id === id
          ? { ...char, displayEpisodes: !char.displayEpisodes }
          : char
      )
    );
  }

  function setSpeciesCheck(species) {
    console.log("species : " + species);

    // setSpecies(species);

    setCharacters(
      characters.filter(char =>
        char.species === species
      )
    );
  }

  return (
    <>
      <Header>
        <header>
          <h1>
            Rick and Morty <span>React</span> App
          </h1>
        </header>
        <ul>
          <Link to={`/${getPage(-1, page)}`}>
            <li> prev </li>
          </Link>
          {clickablePages.map(pageNumber => (
            <Link to={`/${pageNumber}`} key={pageNumber}>
              <PageNumber actualPage={pageNumber === page}>
                {pageNumber}
              </PageNumber>
            </Link>
          ))}
          <Link to={`/${getPage(1, page)}`}>
            <li> next </li>
          </Link>
        </ul>
      </Header>
      <Menu>
        <h1>Filter</h1>
        <BorderBox>
          <h3>Species</h3>
          <input type="checkbox" name="Human" value="Human" checked={speciesChecked} onChange={() => setSpeciesCheck("Human")} /> Human <br/>
          <input type="checkbox" name="Alien" value="Alien" checked={speciesChecked} onChange={() => setSpeciesCheck("Alien")} /> Alien <br/>
          <input type="checkbox" name="Other" value="Other" checked={speciesChecked} onChange={() => setSpeciesCheck("Other")} /> Other <br/>
        </BorderBox>
        <BorderBox>
          <h3>Gender</h3>
          <input type="checkbox" name="Male" value="Male" /> Male <br/>
          <input type="checkbox" name="Female" value="Female" /> Female <br/>
        </BorderBox>
        <BorderBox><h3>Origin</h3></BorderBox>
      </Menu>
      <CardGrid loading={loading}>
        {loading ? (
          <Spinner />
        ) : (
          characters.map(char => (
            <Card
              key={char.id}
              onClick={() => setDisplayEpisodes(char.id)}
              displayEpisodes={char.displayEpisodes}
            >
              <img src={char.image} alt={char.name} />
              <section>
                <header>
                  <h1>
                    <span>{char.id}</span> {char.name}
                  </h1>
                  <h3>
                    Species - {char.species}
                  </h3>
                  <h3>
                    Status - {char.status}
                  </h3>
                  <h3>
                    Gender - {char.gender}
                  </h3>
                  <h3>
                    URL - {char.url}
                  </h3>
                  <h3>
                    Created - {char.created}
                  </h3>
                </header>
              </section>
              <ul>
                <p>Episodes:</p>
                {char.episode
                  .map(epi => epi.split('/episode/')[1])
                  .map(epi => (
                    <li key={char.id + epi}>{epi}</li>
                  ))}
              </ul>
            </Card>
          ))
        )}
      </CardGrid>
    </>
  );
}
